// ---------- You can play with these ----------
var focal = 1;
var timeScale = 1;
var duration = 2;
var defaults = {
    bigRot: { radius: .45, endAngle: 2 * Math.PI, angleOffset: 0 },
    smallRot: { radius: .12, endAngle: 12 * Math.PI, angleOffset: 0 },
    translationZ: { from: 9, to: -1, offset: 0 },
    particles: {
        perSecond: 250,
        color: function() { return [~~(90 + Math.random() * 100),~~(80 + Math.random() * 40),~~(100+Math.random() * 155)]; },
        //color: function(pos) { return rainbow(1.2 - pos[2] / 7); },
        radius: {min: .005, max: .015},
        decay: {min: .6, max: .8},
        initialSpacing: .015,
        dispersion: { min: 0, max: .05 },
        momentumInfluence: [.2,.2,.4],
        extraSpeed: [0,0,0]
    },
    autoMomentum: false // When false only the big rotation movement will be taken into account
};
var sources = [
    alter(defaults, {  }),
    alter(defaults, { smallRot: { angleOffset: Math.PI } }),
    alter(defaults, { translationZ: { offset: -2.5 }, bigRot: { angleOffset: Math.PI } }),
    alter(defaults, { translationZ: { offset: -2.5 }, bigRot: { angleOffset: Math.PI }, smallRot: { angleOffset: Math.PI } }),
    alter(defaults, { translationZ: { offset: -5 } }),
    alter(defaults, { translationZ: { offset: -5 }, smallRot: { angleOffset: Math.PI } }),
    alter(defaults, { translationZ: { offset: -7.5 }, bigRot: { angleOffset: Math.PI } }),
    alter(defaults, { translationZ: { offset: -7.5 }, bigRot: { angleOffset: Math.PI }, smallRot: { angleOffset: Math.PI } })
];
// ---------------------------------------------

function alter(a, b)
{
    var r = {};
    for(var prop in a) if(a.hasOwnProperty(prop))
    {
        if(typeof a[prop] === 'object')
            r[prop] = alter(a[prop], b && b[prop]);
        else
            r[prop] = (b && b[prop] !== undefined) ? b[prop] : a[prop];
    }
    return r;
}

function rainbow(v)
{
    v = 6 * (v - Math.floor(v));
    var r = v + 2, g = v, b = v - 2;
    r = r < 3 ? 1 : r < 4 ? 4 - r : r < 6 ? 0 : r < 7 ? r : 1;
    g = g < 1 ? g : g < 3 ? 1 : g < 4 ? 4 - g : 0;
    b = b < 0 ? 0 : b < 1 ? b : b < 3 ? 1 : 4 - b;
    return [~~(255 * r), ~~(255 * g), ~~(255 * b)];
}

var _debug = document.getElementById('debug');
function debug(v)
{
    _debug.textContent = v;
}

var c = document.getElementById('c'),
    ctx = c.getContext('2d');

function onResize()
{
    c.width = c.offsetWidth;
    c.height = c.offsetHeight;
    ctx.translate(c.width * .5, c.height * .5);
    var s = Math.max(c.width, c.height) * .5;
    ctx.scale(s, s);
}
window.addEventListener('resize', onResize);
onResize();

function random(a,b)
{
    return a + Math.random() * (b - a);
}
function spherical(r, a, b, origin)
{
    var o = origin || [0,0,0];
    var tmp = r * Math.sin(a);
    return [o[0] + r * Math.cos(a), o[1] + tmp * Math.cos(b), o[2] - tmp * Math.sin(b)];
}
function projection(p, f)
{
    // Behind the viewer ?
    if(p[2] <= -f) return [NaN, NaN];
    var m = f / (p[2] + f);
    return [p[0] * m, p[1] * m];
}
function lerp(p, from, to)
{
    return from + p * (to - from);
}
function lerpOffset(p, from, to, offset)
{
    var d = (to - from),
        op = p + offset / d;
    while(op < 0) op++;
    while(op > 1) op--;
    return from + op * d;
}

function setProp(prop) { return function(val) { this[prop] = val; return this; } };

function Particle()
{
    this.color = [210, 100, 10];
    this.rad = .01;
    this.pos = [0,0,0];
    this.speed = [0,0,0];
    this.decay = .7;
    this.life = 1;
    this.lastTime = Date.now();
}
Particle.prototype.setColor = setProp('color');
Particle.prototype.setRadius = setProp('rad');
Particle.prototype.setSpeed = setProp('speed');
Particle.prototype.setPosition = setProp('pos');
Particle.prototype.setDecay = setProp('decay');
Particle.prototype.step = function()
{
    var time = Date.now(),
        delta = (time - this.lastTime) * .001 * timeScale;
    this.lastTime = time;
    this.pos[0] += this.speed[0] * delta;
    this.pos[1] += this.speed[1] * delta;
    this.pos[2] += this.speed[2] * delta;
    this.life -= this.decay * delta;
    if(this.life <= 0) return false;
};
Particle.prototype.draw = function()
{
    var pos2d = projection(this.pos, focal);
    var rad = projection([this.rad,0,this.pos[2]], focal)[0];
    var opacity = 1 - this.life;
    opacity = 1 - opacity * opacity;
    //// Lighter = slightly slow
    //ctx.globalCompositeOperation = 'lighter';
    //// No alpha = faster !!!
    ctx.fillStyle = 'rgb('+this.color[0]+','+this.color[1]+','+this.color[2]+')';
    //ctx.fillStyle = 'rgba('+this.color[0]+','+this.color[1]+','+this.color[2]+','+opacity+')';
    //// Circles = slow !
    //ctx.beginPath();
    //ctx.arc(pos2d[0], pos2d[1], rad, 0, 2 * Math.PI);
    //ctx.fill();
    var halfRad = rad * .5;
    ctx.fillRect(pos2d[0] - halfRad, pos2d[1] - halfRad, rad, rad);
};

function Emitter(particles)
{
    this.particles = particles;
    this.pos = [0,0,0];
    this.counter = 0;
    this.speed = 300;
    this.maxCounter = 10;
    this.lastTime = Date.now();
    this.size = .015;
    this.autoMomentum = false;
    this.lastPos = [0,0,0];
    this.momentum = [0,0,0];
    this.momentumEmission = [0,0,0];
    this.particleColor = function() { return [255,255,255]; };
    this.particleRadius = {min: .005, max: .015};
    this.particleDecay = {min: .6, max: .8};
    this.particleDispersion = {min: 0, max: .05};
    this.extraEmissionSpeed = [0,0,0];
}
Emitter.prototype.emit = function()
{
    var pos = spherical(random(0, this.size), random(0, 2 * Math.PI), random(0, 2 * Math.PI), this.pos);
    var dispSpeed = random(this.particleDispersion.min, this.particleDispersion.max),
        dispAngle = random(0, 2 * Math.PI);
    var speed = [
        this.momentum[0] * this.momentumEmission[0] + dispSpeed * Math.cos(dispAngle) + this.extraEmissionSpeed[0],
        this.momentum[1] * this.momentumEmission[1] + dispSpeed * Math.sin(dispAngle) + this.extraEmissionSpeed[1],
        this.momentum[2] * this.momentumEmission[2] + this.extraEmissionSpeed[2]];
    this.particles.push(
        new Particle()
            .setPosition(pos)
            .setSpeed(speed)
            .setRadius(random(this.particleRadius.min, this.particleRadius.max))
            .setDecay(random(this.particleDecay.min, this.particleDecay.max))
            .setColor(this.particleColor(pos))
    );
};
Emitter.prototype.step = function()
{
    if (!stop) {
    var time = Date.now(),
        delta = (time - this.lastTime) * .001 * timeScale;

        this.lastTime = time;
        if (this.autoMomentum)
            this.momentum = [(this.pos[0] - this.lastPos[0]) / delta, (this.pos[1] - this.lastPos[1]) / delta, (this.pos[2] - this.lastPos[2]) / delta];
        this.lastPos = this.pos.slice();
        this.counter += this.speed * delta;
        if (this.counter > this.maxCounter) this.counter = this.maxCounter;
        while (this.counter >= 1) {
            this.counter--;
            this.emit();
        }
    }
};

var particles = [];
var emitters = [];
(function()
{
    for(var i = 0, l = sources.length; i < l; i++)
    {
        var s = sources[i],
            e = new Emitter(particles);
        e.speed = s.particles.perSecond;
        e.particleColor = s.particles.color;
        e.size = s.particles.initialSpacing;
        e.particleDispersion = s.particles.dispersion;
        e.particleRadius = s.particles.radius;
        e.particleDecay = s.particles.decay;
        e.momentumEmission = s.particles.momentumInfluence;
        e.extraEmissionSpeed = s.particles.extraSpeed;
        e.autoMomentum = s.autoMomentum;
        emitters.push(e);
    }
})();
var endcount = 0;
//var fpsCounter = { frequency: 500, elem: document.getElementById('fps'), lastUpdate: 0, count: 0 };
function loop()
{
    if (stop) {
        endcount++;
    }
    if (endcount<100) {
        requestAnimationFrame(loop);
    }
    var now = Date.now();
    var t = now * .001 * timeScale;
    ctx.clearRect(-c.width * .5, -c.height * .5, c.width, c.height);
    var p = (t % duration) / duration;
    for(var i = 0, l = sources.length; i < l; i++)
    {
        var s = sources[i], e = emitters[i];
        var a = lerpOffset(p, 0, s.smallRot.endAngle, s.smallRot.angleOffset),
            A = lerpOffset(p, 0, s.bigRot.endAngle, s.bigRot.angleOffset);
        var d = s.bigRot.radius + s.smallRot.radius * Math.cos(a);
        var z = lerpOffset(p, s.translationZ.from, s.translationZ.to, s.translationZ.offset);
        e.pos = [d * Math.cos(A), d * Math.sin(A), z + s.smallRot.radius * Math.sin(a)];
        if(!e.autoMomentum)
            e.momentum = [s.bigRot.radius * -Math.sin(A), s.bigRot.radius * Math.cos(A), (s.translationZ.to - s.translationZ.from) / duration];
        e.step();
    }

    // debug(particles.length);

    for(var i = particles.length; i --> 0; )
    {
        if(particles[i].step() === false)
            particles.splice(i, 1);
        else
            particles[i].draw();
    }

    //fpsCounter.count++;
    //var fpsTimeDiff = now - fpsCounter.lastUpdate;
    //if(fpsTimeDiff >= fpsCounter.frequency)
    //{
    //    var fpsVal = 1000 * fpsCounter.count / fpsTimeDiff;
    //    fpsCounter.lastUpdate = now;
    //    fpsCounter.count = 0;
    //    fpsCounter.elem.textContent = fpsVal.toFixed(2);
    //}
}

var stop = false;
requestAnimationFrame(loop);

function endIntro(){
    stop = true
}