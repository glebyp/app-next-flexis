import angular from 'angular';
//import StartCtrl from './startCtrl';

import intro from '../parts/intro/intro';
import navbar from '../parts/navbar/navbar';
import all from '../parts/all/all';
import detal from '../parts/detal/detal';
import install from '../parts/install/install';

import Auth from '../services/auth';
import Data from '../services/data';


let startModule = angular.module('startModule', [
    'ui.router',
    'ngCookies',
    'ct.ui.router.extras.dsr',
    'ct.ui.router.extras.statevis'])
    .controller(...intro)
    .controller(...navbar)
    .controller(...all)
    .controller(...detal)
    .controller(...install)
    .service(...Auth)
    .service(...Data)
    .config(function ($stateProvider, $urlRouterProvider) {
        console.log("start route congigured");
        var states = [];
            states.push({
        name: "intro",
            url: "/",
            views: {
            "app": route('intro')
        }
    })
states.push({
    name: "start",
    url: "/",
    views: {
        "app@": {
            templateUrl: "app/start/start.html"
        }
    }
})
states.push({
    name: "start.all",
    url: "all",
    views: {
        "navbar": route("navbar"),
        "main": route("all")
    }
})
states.push({
    name: "start.detal",
    url: "o/:id",
    views: {
        "navbar": route("navbar", "navbar-detal"),
        "main": route("detal")
    }
})
states.push({
    name: "install",
    url: "/i/:id/:build",
    views: {
        "app": route(`install`)
    }
})
$urlRouterProvider.otherwise('/');
        states.forEach(s=>$stateProvider.state(s));
    });
function route(name, view) {
    if (!view) view = name
    return {
        templateUrl: 'app/parts/' + name + '/' + view + '.html',
        controller: name
    }
}


console.log("startModule initialized");

export default startModule;