class Dic {
    constructor() {
        this.file = {
            "apk": {
                icon: "android",
                color: "green",
                text: "Загрузить для Android"
            },
            "ipa": {
                icon: "apple",
                color: "black",
                text: "Установить на iOS"
            },
            "exe": {
                icon: "windows",
                text: "Загрузить приложение"
            },
            "dmg": {
                icon: "apple",
                text: "Загрузить установщик"
            },
            "pdf": {
                icon: "file pdf outline",
                text: "Загрузить установщик"
            },
            "zip": {
                icon: "file archive outline",
                color: "yellow",
                text: "Загрузить архив"
            },
            "air": {
                icon: "cloud outline",
                color: "red",
                text: "Загрузить приложение"
            },
            "page": {
                icon: "world",
                text: "Открыть страницу"
            },
            "win": {
                icon: "windows",
                color: "blue",
                text: "Скачать архив"
            },
            "*": {
                icon: "file",
                color: "violet",
                text: "Загрузить файл"
            },
        };

        this.depo = {
            "dev": {
                icon: "settings",
                color: "red"
            },
            "development": {
                icon: "settings",
                color: "red"
            },
            "sales": {
                icon: "ruble",
                color: "green"
            },
            "prod": {
                icon: "users",
                color: "orange",
            },
            "lab": {
                icon: "lab",
                color: "violet"
            },
            "*": {
                color: "none"
            }
        };
    }
    
    
}

var d = new Dic()
export default d;