import R from 'ramda'
import moment from 'moment'
import D from './dic'



function newArtifact(ext) {
    var artifact = D.file[ext];
    if (!artifact) {
        artifact = D.file["*"];
    }
    return jQuery.extend({}, artifact);
}


export class AppCard {
    constructor(a) {
        $.extend(this, D.depo[a.depo]);
        if (a.type) {
            a.depo = a.type;
        }
        for (var prop in a) {
            this[prop] = a[prop];
        }

        this.artifacts = [];
        for (var ext in a.extTypes) {
            var artifact = newArtifact(ext);
            artifact.url = a.extTypes[ext];
            this.artifacts.push(artifact);
        }
        this.sortDate = new Date(a.date).getTime();
        this.date = moment(this.sortDate).fromNow();

    }

    get version() {
        return this.v ? this.v + " / " + this.build : this.build;
    }

    get title() {
        return this.name ? this.name : this.id;
    }
}


