//import R from 'ramda'
import moment from 'moment'
import D from './dic'

export class Artifacts {
    constructor(array) {
        let first = array[0];
        this.v = first.v;
        this.build = first.build;
        this.sortDate = new Date(first.date).getTime();
        this.date = moment(this.sortDate).format("D MMMM YYYY HH:mm");
        this.arts = array.map((a) => {
            $.extend(a, D.file[a.ext]);
            if (a.name.length > 26) a.title = a.name.slice(1, 24) + "..."
            else a.title = a.name;
            return a
        })
    }

    get version() {
        return this.v ? this.v : this.build;
    }

}

/*
 "ext": "apk",
 "date": "2015-10-16T12:13:00.708Z",
 "build": 110,
 "v": "3.0.70",
 "name": "3070-ahk-mobarm.apk",
 "url": "https://app.flexis.ru/ru.idecide.axk/3070-ahk-mobarm.apk"
 */


