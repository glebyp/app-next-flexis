import angular from 'angular';
import 'jquery';
import 'angular-cookies';
import "angular-ui-router";
import "angular-360-no-scope";
import "angular-loading-bar";
import "ui-router-extras";
import "matreshka";

import 'semantic-ui';

import './css/all.css!';
import './css/intro.css!';
import './css/normalize.css!';
import './css/animate.css!';

import startModule  from './start/start';

angular.module('appStart', ["angular-loading-bar",
        startModule.name
    ])
.run(['$rootScope', '$state', '$stateParams', function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
}]);


var body = document.getElementsByTagName('body').item(0) || document.documentElement;
body.setAttribute("ng-app", "appStart");
angular.element(document).ready(function () {
    console.log("document ready");
    angular.bootstrap(document, ['appStart']);
    endIntro();
});
