import GeoPattern from 'geopattern'
import Matreshka from 'matreshka';
import flyd from 'flyd'
import R from 'ramda'

class IntroCtrl extends Matreshka {
    constructor(auth, $state) {
        console.log("IntroCtrl");
        super();

        this.bindNode('code', '#input-code');
        this.on('change:code', (v)=> {
            $('#lock-icon').transition("hide");
            if (v.value.length > 3) {
                auth.checkCode(v.value);
            }
        });

        let killer = flyd.stream();
        flyd.endsOn(killer.end, flyd.map(
            R.cond([
                [R.equals("error") , ()=>$('#lock-icon').transition('horizontal flip in')],
                [R.isEmpty, ()=> console.log("key is empty")],
                [R.T, ()=>{
                    $state.go("start.all");
                    killer.end(true);
                }]
            ]), auth.result
        ));

        $('#lock-icon').transition("hide");
        $('#geopattern').geopattern(new Date().toDateString())
    }
}
export default ['intro', IntroCtrl];