//import ZeroClipboard from 'zeroclipboard'
class DetalCtrl {
    constructor(auth, data, $scope, $stateParams) {
        console.log("::DetalCtrl id",$stateParams.id);
        data.loadBuilds($stateParams.id);

        $scope.copyURL = (art) => {
            var url = ""
            if (art.ext == "ipa"){
                url = `${location.host}/#/i/${$stateParams.id}/${art.build}`;
            } else {
                url = art.url
            }
            window.prompt("Ссылка для оправки", url);
        }
        $scope.window
    }
}
export default ['detal', DetalCtrl];