import flyd from "flyd"
import R from 'ramda'

class InstallCtrl {
    constructor(data, $stateParams, $scope) {
        $scope.dimmer = true;

        flyd.on((builds)=> {
            var b = R.find(
                R.propEq('build', Number.parseInt($stateParams.build))
            )(builds.ios);

            window.open(b.url);
            $scope.url = b.url;
            $scope.dimmer = false;
            $('.modal')
                .modal({
                    closable  : false,
                    onDeny    : function(){
                        window.open(b.url);
                        return false;
                    }
                })
                .modal('show')
            ;
            $scope.filename = b.name;

        }, data.installBuild($stateParams.id));

        $('.ui.modal')
            .modal()
        ;

    }
}
export default ['install', InstallCtrl]

