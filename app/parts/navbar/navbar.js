class NavBar {
    constructor($scope, $rootScope, $state, data, auth) {
        console.log(":NavBar");
        $scope.out = () => {
            console.log("out");
            data.clear();
            auth.clear();
            $state.go("intro")
        }
        $scope.home = () => {
            $state.go("start.all")
        }

        $rootScope.detal = false;
        var detal = $("#detal").first();
        detal.checkbox({
            onChange: ()=> {
                $rootScope.detal = detal.checkbox('is checked');
                $rootScope.$apply();
            }
        })

        auth.check()
    }
}

export default ['navbar', NavBar];