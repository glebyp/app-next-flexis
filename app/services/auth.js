import flyd from "flyd"
import R from 'ramda'

let code = flyd.stream();
let result = flyd.stream();

let xrq
function request(code) {
    if (xrq) xrq.abort();
    xrq = $.ajax({
        url: 'https://app.flexis.ru/_codes/' + code + '.json',
        cache: false,
        dataType: "json",
        error: (x, e)=> {
            if (e == "error") result(e)
        },
        success: result
    });
}

let isValid = (x, fn) => {
    var v = !((R.isEmpty(x) || R.isNil(x)));
    if (v) if (fn) fn(x)
    return v;
}

class Auth {
    constructor($cookies,$state) {
        console.log("::::Auth");

        this.keys = flyd.combine((r)=> {
            $cookies.put("keys", r());
            $cookies.put("code", code());
            return r().keys
        }, [result]);

        this.$cookies = $cookies;
        isValid($cookies.get("code"), code);
        isValid($cookies.get("keys"), result);
        this.check = function () {
            if (!isValid(code(),request)) {
                $state.go("intro")
            }
        }


        if (!$cookies.get("firstrun")){
            console.log("firstrun");
            $cookies.put("firstrun",false)
            this.checkCode("demo")
        }

    }

    get result() {return result}



    checkCode(value) {
        code(value);
        request(value);
    }

    clear() {
        code(null);
        result("");
        this.$cookies.remove("code");
        this.$cookies.remove("keys");
    }
}

export default ['auth', Auth];