import flyd from 'flyd'
import R from 'ramda'
import {AppCard} from '../classes/appcard'
import {Artifacts} from '../classes/artifacts'


function load(url, channel) {
    dimmer = true;
    $.ajax({
        url: url,
        cache: false,
        dataType: "json",
        success: channel,
        error: (e, t) => {
            console.log(t, url);
        }
    });
}


let apps = null
let builds = null
let info = null

let buildsCache = new Map();
let infoCache = new Map();

let currentID = flyd.stream();
flyd.on((id)=> {
    builds = buildsCache.has(id) ? buildsCache.get(id) : null
    info = infoCache.has(id) ? infoCache.get(id) : null
}, currentID)

let infoStream = flyd.stream();
flyd.on((data)=> {
    info = new AppCard(data);
    infoCache.set(currentID(), data);
}, infoStream);

let allapps = flyd.stream();
let allappsInited = false;
let buildsStream = flyd.stream();

class Data {
    constructor(auth, $rootScope) {
        console.log("::::Data");

        flyd.combine(function (app, keys) {
            var doit = R.ifElse(
                R.identical('*'),
                (x)=>app(),
                ()=> {
                    let contains = (a, b) => R.match(b, a).length > 0;
                    let pickbyKey = (obj, id) => R.containsWith(contains, id, keys());
                    return R.pickBy(pickbyKey, app());
                }
            );
            let userapps = doit(keys());
            apps = Object.values(userapps).map(a=> new AppCard(a));
            $rootScope.$apply();
        }, [allapps, auth.keys]);

        flyd.on((b)=> {
            let idBuilds = R.groupBy((g)=>g.build, R.unnest(Object.values(b)));
            builds = Object.values(idBuilds).map((x)=> new Artifacts(x));
            buildsCache.set(currentID(), builds);
            $rootScope.$apply();
        }, buildsStream);

        $rootScope.data = this;
    }

    loadBuilds(id) {
        currentID(id);
        if (!builds){
            load(`https://app.flexis.ru/${id}/info.json`, infoStream);
            load(`https://app.flexis.ru/${id}/build.json`, buildsStream);
        }
    }

    installBuild(id) {
        var iosStream = flyd.stream();
        load(`https://app.flexis.ru/${id}/build.json`, iosStream);
        return iosStream;
    }

    get apps() {
        if (!allappsInited){
            allappsInited = true;
            load("https://app.flexis.ru/all.json", allapps);
        }
        return apps
    }

    get builds() {
        return builds
    }

    get info() {
        return info
    }

    clear() {
        apps = null;
        builds = null
        info = null
    }
}

export default ['data', Data];